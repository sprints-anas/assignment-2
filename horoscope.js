program  = () => {
    askForName();
}
askForName = () => {
    let name = prompt("Enter your name: ");
    while (!/^[a-z ]+$/i.test(name)) {
        name = prompt("Enter your name: ");
    }

    let password = askForPassword();
    if (password != 1) {
        let zodiacSign = askForBirthMonthDay();
        document.getElementById("welcome-user").textContent = `Hello ${name} ✌. Your zodiac sign is ${zodiacSign}`;
        alert(`Hello ${name} ✌. Your zodiac sign is ${zodiacSign}`);
    }
}

askForPassword = () => {
    let count = 0;
    while (prompt("Enter your Password: ") != 123) {
        count++;
        if (count > 2) {
            alert("You’ve entered wrong password 3 times");
            return 1;
        }
    }
}

askForBirthMonthDay = () => {
    let birthMonth = prompt("Enter your birth month [1 - 12]: ");
    while (birthMonth < 1 || birthMonth > 12) {
        birthMonth = prompt("Enter your birth month [1 - 12]: ");
    }

    let birthDay = prompt("Enter your birth day [1 - 30]: ");
    while (birthDay < 1 || birthDay > 30) {
        birthDay = prompt("Enter your birth month [1 - 30]: ");
    }

    return getZodiacSign(birthDay, birthMonth);
}

getZodiacSign = (birthDay, birthMonth) => {
    let zodiacSign;
    if (birthMonth == 1 && birthDay >= 20 || birthMonth == 2 && birthDay <= 18) {
        zodiacSign = ("Aquarius ♒");
    }

    else if (birthMonth == 2 && birthDay >= 19 || birthMonth == 3 && birthDay <= 20) {
        zodiacSign = ("Pisces ♓");
    }

    else if (birthMonth == 3 && birthDay >= 21 || birthMonth == 4 && birthDay <= 19) {
        zodiacSign = ("Aries ♈");
    }

    else if (birthMonth == 4 && birthDay >= 20 || birthMonth == 5 && birthDay <= 20) {
        zodiacSign = ("Taurus ♉");

    }

    else if (birthMonth == 5 && birthDay >= 21 || birthMonth == 6 && birthDay <= 20) {
        zodiacSign = ("Gemini ♊");
    }

    else if (birthMonth == 6 && birthDay >= 21 || birthMonth == 7 && birthDay <= 22) {
        zodiacSign = ("Cancer ♋");
    }
    
    else if (birthMonth == 7 && birthDay >= 23 || birthMonth == 8 && birthDay <= 22) {
        zodiacSign = ("Leo ♌");
    }
    
    else if (birthMonth == 8 && birthDay >= 23 || birthMonth == 9 && birthDay <= 22) {
        zodiacSign = ("Virgo 🈳");
    }
    
    else if (birthMonth == 9 && birthDay >= 23 || birthMonth == 10 && birthDay <= 22) {
        zodiacSign = ("Libra ♎");
    }

    else if (birthMonth == 10 && birthDay >= 23 || birthMonth == 11 && birthDay <= 21) {
        zodiacSign = ("Scorpio ♏");
    }

    else if (birthMonth == 11 && birthDay >= 22 || birthMonth == 12 && birthDay <= 21) {
        zodiacSign = ("Sagittarius ♐");
    }

    else if (birthMonth == 12 && birthDay >= 22 || birthMonth == 1 && birthDay <= 19) {

        zodiacSign = ("Capricorn ♑");
    }
    return zodiacSign;
}